#!/usr/bin/env bash

BUILD_NUMBER=$1

echo "Deployment started..."
echo "Creating new deployment with image $GO_PIPELINE_LABEL"

sed "s/{{BUILD_NUMBER}}/${GO_PIPELINE_LABEL}/" ./deployment-template.yml >> ./deployment.yml

cat ./deployment.yml

kubectl apply -f deployment.yml
kubectl apply -f service.yml

rm -rf ./deployment.yml

echo "Deployment done..."